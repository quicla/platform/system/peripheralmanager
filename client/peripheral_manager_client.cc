/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <peripheralmanager/peripheral_manager_client.h>

#include <android/os/IPeripheralManager.h>
#include <base/logging.h>
#include <binder/IBinder.h>
#include <binderwrapper/binder_wrapper.h>

namespace android {

PeripheralManagerClient::PeripheralManagerClient() {}

PeripheralManagerClient::~PeripheralManagerClient() {}

bool PeripheralManagerClient::Init() {
  const char kServiceName[] = "android.os.IPeripheralManager";
  sp<IBinder> pmanager_binder = BinderWrapper::Get()->GetService(kServiceName);
  if (!pmanager_binder.get()) {
    LOG(ERROR) << "Didn't get " << kServiceName << " service";
    return false;
  }
  pmanager_ = interface_cast<os::IPeripheralManager>(pmanager_binder);
  return true;
}

int PeripheralManagerClient::AddInts(int a, int b) {
  DCHECK(pmanager_.get());
  int result = 0;
  pmanager_->AddInts(a, b, &result);
  return result;
}

}  // namespace android
