/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "peripheral_manager.h"

#include <base/logging.h>
#include <binderwrapper/binder_wrapper.h>

namespace android {

PeripheralManager::PeripheralManager() {}

PeripheralManager::~PeripheralManager() = default;

bool PeripheralManager::Init() {
  String8 interface_desc(getInterfaceDescriptor());
  return BinderWrapper::Get()->RegisterService(interface_desc.string(), this);
}

Status PeripheralManager::AddInts(int32_t a, int32_t b, int32_t* _aidl_return) {
  *_aidl_return = a + b;
  return Status::ok();
}

}  // namespace android
